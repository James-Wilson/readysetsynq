"use strict";
let websocket = null;
let clientID = 0;

function generateUUID() {
  let d = new Date().getTime();
  let d2 = (performance && performance.now && (performance.now() * 1000)) || 0;//Time in microseconds since page-load or 0 if unsupported
  let uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    let r = Math.random() * 16;
    if (d > 0) {
      r = (d + r) % 16 | 0;
      d = Math.floor(d / 16);
    } else {
      r = (d2 + r) % 16 | 0;
      d2 = Math.floor(d2 / 16);
    }
    return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
  });
  return uuid;
};

function connect(hostname, port) {
  let serverUrl;
  let scheme = "ws";

  if (document.location.protocol === "https:") {
    scheme += "s";
  }

  serverUrl = scheme + "://" + hostname + ":" + port;

  websocket = new WebSocket(serverUrl);
  websocket.onopen = function (evt) { onOpen(evt) };
  websocket.onclose = function (evt) { onClose(evt) };
  websocket.onmessage = function (evt) { onMessage(evt) };
  websocket.onerror = function (evt) { onError(evt) };
  console.log("***CREATED WEBSOCKET");
}

function onOpen(evt) {
  console.log("***ONOPEN");
  $('#status').html("Connected").addClass("online").removeClass("offline");
  $('#divCSV').show();
  send({ state: "requestAll" });
  resetUI();
  resetSelect();
  send({ state: "ping" });
}

function onClose(evt) {
  console.log("***ONCLOSE");
  $('#status').html("Disconnected").addClass("offline").removeClass("online");
  $('#divCSV').hide();
  resetUI();
  resetSelect();
}

function onMessage(evt) {
  let data = JSON.parse(evt.data);

  switch (data.state) {
    case "getCSVs":
      let files = data.message.split(",");
      let select = document.getElementById("csvName");
      resetSelect();

      files.forEach(element => {
        let option = document.createElement("option");
        option.value = option.text = element;
        select.add(option);
      });
      break;
    case "loadCSV":
      $('#btnUpdate').prop('disabled', true);
      $('#btnAddRow').hide();
      $("#tblData").html("");
      processData(data.message);
      break;
  }
}

function onError(evt) {
  console.log("***ONERROR");
  console.log(evt.data);
}

function disconnect() {
  if (websocket == null)
    return;
  websocket.disconnect();
}

function send(data) {
  data = data || {};
  data.id = clientID,
  data.date = Date.now();

  websocket.send(JSON.stringify(data));
}

$(document).ready(function () {
  clientID = generateUUID();
  $('#divCSV').hide();
  $('#btnUpdate').prop('disabled', true);
  $('#btnAddRow').hide();
  document.getElementById("btnConnect").addEventListener("click", connectToWS);
  document.getElementById("btnUpdate").addEventListener("click", updateCSV);
  document.getElementById("csvName").addEventListener("change", selectCSV);
});

function resetUI() {
  $('#btnUpdate').prop('disabled', true);
  $('#btnAddRow').hide();
  $("#tblData").html("");
}

function resetSelect() {
  $("#csvName").find('option').not(':first').remove();
}

function removeFirstIfComma(str) {
  return (str[0] == ',') ? str.substr(1) : str;
}

function processData(allText) {
  let obj = JSON.parse(allText),
    headers = Object.keys(obj[0]),
    values = Object.values(obj),
    table = "<thead><tr>";

  headers.forEach((item, index) => {
    table += "<th>";
    table += "<input class='dataTable heading' type='text' name='heading" + index + "' value='" + item + "'>";
    table += "</th>";
  });

  // Add final options header with delete button
  table += "<th><p class='dataTable heading'>- Options -</p></th>";
  table += "</tr></thead>";
  values.forEach((item, index) => {
    table += "<tr id=row" + index + ">";
    for (let j = 0; j < headers.length; j++) {
      let val = item[headers[j]];
      if (val == undefined || val == null)
        val = "";
      table += "<td>";
      table += "<input class='dataTable row' type='text' name='row" + index + "-heading-" + (j + 1) + "' value='" + val + "'>";
      table += "</td>";
    }
    // Add final Options buttons
    table += "<td>"
    table += "<button class='dataTable btnUp' name='row" + index + "-up' onclick='moveRowUp(" + index + ")'>Up</button>";
    table += "<button class='dataTable btnDown' name='row" + index + "-down' onclick='moveRowDown(" + index + ")'>Down</button>";
    table += "<button class='dataTable btnDelete' name='row" + index + "-delete' onclick='deleteRow(" + index + ")'>Delete</button>";
    table += "</td></tr>";
  });
  table += "";
  if (values.length > 0)
    $('#btnAddRow').show();
  $("#tblData").html(table);
  $('#btnUpdate').prop('disabled', false);
};

function moveRowUp(id) {
  let row = $(document.getElementById("row" + id));

  if (row !== undefined)
    row.insertBefore(row.prev());
}

function moveRowDown(id) {
  let row = $(document.getElementById("row" + id));

  if (row !== undefined)
    row.insertAfter(row.next());
}

function deleteRow(id) {
  if (!confirm("Please confirm that you want to delete row #" + id + "?"))
    return;

  let row = document.getElementById("row" + id),
    table = row.parentNode;

  while (table && table.tagName != 'TABLE')
    table = table.parentNode;
  if (!table)
    return;
  table.deleteRow(row.rowIndex);
}

function addRow() {
  let tbl = $('#tblData');
  if (tbl == undefined)
    return;
  let columnCount = tbl[0].rows[0].cells.length;
  if (columnCount > 0) {
    let rowCount = tbl[0].rows.length;
    let tableRow = "<tr id=row" + rowCount + ">";
    for (let i = 1; i < columnCount; i++) {
      tableRow += "<td>";
      tableRow += "<input class='dataTable row' type='text' name='row" + rowCount + "-heading-" + i + "' value=''>";
      tableRow += "</td>";
    }
    // Add final Options buttons
    tableRow += "<td>"
    tableRow += "<button class='dataTable btnUp' name='row" + rowCount + "-up' onclick='moveRowUp(" + rowCount + ")'>Up</button>";
    tableRow += "<button class='dataTable btnDown' name='row" + rowCount + "-down' onclick='moveRowDown(" + rowCount + ")'>Down</button>";
    tableRow += "<button class='dataTable btnDelete' name='row" + rowCount + "-delete' onclick='deleteRow(" + rowCount + ")'>Delete</button>";
    tableRow += "</td></tr>";
    $('#tblData > tbody:last-child').append(tableRow);
  }
}

function csvJSON(csv) {
  const lines = csv.split('\n')
  const result = []
  const headers = lines[0].split(',')

  for (let i = 1; i < lines.length; i++) {        
      if (!lines[i])
          continue
      const obj = {}
      const currentline = lines[i].split(',')

      for (let j = 0; j < headers.length; j++) {
          obj[headers[j]] = currentline[j]
      }
      result.push(obj)
  }
  return result
}

function tableToText(tableId) {
  let divCSV = "";

  $('#' + tableId + ' tr').each(function () {
    // Loop through all columns
    let columnData = "";
    $(this).find('th').each(function () {
      if ($(this)[0] !== undefined && $(this)[0].children[0] !== undefined) {
        let child = $(this)[0].children[0];
        if (child.innerText != "- Options -")
          columnData += ',' + child.value;
      }
    });
    // Ignore empty data
    if (columnData && columnData !== "")
      divCSV += removeFirstIfComma(columnData);

    // Loop through all rows
    let rowData = "";
    $(this).find('td').each(function () {
      if ($(this)[0] !== undefined && $(this)[0].children[0] !== undefined) {
        let child = $(this)[0].children[0];
        // Ignore the option buttons
        if (child.type == "text") {
          rowData += ',' + child.value;
        }
      }
    });
    // Ignore empty data
    if (rowData && rowData !== "")
      divCSV += "\n" + removeFirstIfComma(rowData);
  });
  return divCSV;
}

function connectToWS() {
  let hostname = document.getElementById("hostname");
  let port = document.getElementById("port");
  // Make sure a host name was entered
  if (!hostname || hostname.value.trim() == "" || !port || port.value.trim() == "")
    return;

  $('#status').html("...").removeClass("online").removeClass("offline");
  connect(hostname.value.trim(), port.value.trim());
}

function updateCSV() {
  let text = tableToText('tblData');
  let elemCsvName = document.getElementById("csvName");
  // Make sure a valid csv option was selected
  if (!elemCsvName.selectedIndex || elemCsvName.selectedIndex <= 0) {
    resetUI();
    return;
  }
  let curFile = elemCsvName.options[elemCsvName.selectedIndex].value;

  send({ state: "setFile", file: curFile, text: csvJSON(text) });
}

function selectCSV() {
  let elemCsvName = document.getElementById("csvName");
  // Make sure a valid csv option was selected
  if (!elemCsvName.selectedIndex || elemCsvName.selectedIndex <= 0)
    return;
  let curFile = elemCsvName.options[elemCsvName.selectedIndex].value;
  
  send({ state: "getFile", file: curFile });
}